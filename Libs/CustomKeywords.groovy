
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject



def static "policyNumber.getPolicyNumber.getQuoteNumber"(
    	String getQuoteNumber	) {
    (new policyNumber.getPolicyNumber()).getQuoteNumber(
        	getQuoteNumber)
}

 /**
	 * Hover over an element.
	 * @param selector (String) CSS selector for the element.
	 */ 
def static "scrollQO.scroll_Code.Se_hover"(
    	String selector	) {
    (new scrollQO.scroll_Code()).Se_hover(
        	selector)
}

 /**
	 * Hover over an element then click it.
	 * @param selector (String) CSS selector for the element.
	 */ 
def static "scrollQO.scroll_Code.Se_hoverClick"(
    	String selector	) {
    (new scrollQO.scroll_Code()).Se_hoverClick(
        	selector)
}


def static "uploadfile.uploadFile.uploadFileToTest"(
    	TestObject to	
     , 	String filePath	) {
    (new uploadfile.uploadFile()).uploadFileToTest(
        	to
         , 	filePath)
}
