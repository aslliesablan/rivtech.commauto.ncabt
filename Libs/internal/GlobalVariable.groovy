package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object NCABT
     
    /**
     * <p></p>
     */
    public static Object STG_Env
     
    /**
     * <p></p>
     */
    public static Object UW_UserName
     
    /**
     * <p></p>
     */
    public static Object UW_Password
     
    /**
     * <p></p>
     */
    public static Object Wait
     
    /**
     * <p></p>
     */
    public static Object DEV_NCABT
     
    /**
     * <p></p>
     */
    public static Object Position_Y
     
    /**
     * <p></p>
     */
    public static Object Position_X
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            NCABT = selectedVariables['NCABT']
            STG_Env = selectedVariables['STG_Env']
            UW_UserName = selectedVariables['UW_UserName']
            UW_Password = selectedVariables['UW_Password']
            Wait = selectedVariables['Wait']
            DEV_NCABT = selectedVariables['DEV_NCABT']
            Position_Y = selectedVariables['Position_Y']
            Position_X = selectedVariables['Position_X']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
